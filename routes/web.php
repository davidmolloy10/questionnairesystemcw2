<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Questionnaires is outside the middleware group as well as inside to allow for the questionnaires page to be accessed without needing a login
 */
Route::resource('questionnaires', 'QuestionnaireController');

Route::group(['middleware' => 'web'], function () {
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');

    //Routing for the questionnaires
    Route::resource('admin/questionnaires', 'QuestionnaireController');

    //Routing for the responses
    Route::resource('admin/responses', 'ResponseController');
    
    //Routing for user pages
    Route::resource('admin/users', 'UserController');  
});





