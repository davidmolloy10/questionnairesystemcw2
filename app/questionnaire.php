<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    //Allows for forms on the application write to these fields in the database while protecting them from mass assignment
    protected $fillable = [
        'title',
        'description',
        'status',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5'
    ];

    //Questionnaire belongs to one user relationship
    public function user() 
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    //Questionnaire belongs to many responses relationship
    public function response() 
    {
        return $this->belongsToMany(response::class);
    }
}
