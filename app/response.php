<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class response extends Model
{   
    /**
     * Allows the response form to write to the database while protecting 
     * the database from mass assignment
     */
    protected $fillable= [
        'questionnaire_id',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5'
    ];

    /**
     * Creates the belongs to one relationship between a response and 
     * a questionnaire
     */
    public function questionnaires() {
        $this->belongsTo(questionnaire::class);
    }
}
