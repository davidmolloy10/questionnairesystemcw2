<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the CSS file to the page-->
    <title>{{ $questionnaire->title }}</title> <!--Uses the questionnaire variable to access the questionnaire title to use that as the page title-->
</head>
<body>
<!--Includes the header in the page-->
@include('includes.header')
<div class="row contentContainter">
    <div class="questionnaireInfo">
        <h1>Questionnaire Name | {{ $questionnaire->title }}</h1>
        <p>Questionnaire Description | {{ $questionnaire->description }}</p>
        <h4>Created by | {{ $questionnaire->user->name }}</h4>
    </div>  
    @if ($errors->any())
      <div class="row errorBox">
          <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    
    <!--Opens a form to save responses-->
    {!! Form::open(array('action' => 'ResponseController@store', 'id' => 'submitResponse')) !!}
    @csrf
    <!--Add a hidden field to pass through the current questionnaire id-->
    {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}

    <div class="row large-12">
        {!! Form::label('question1', $questionnaire->question1) !!}
        {!! Form::radio('question1', '1') !!}
        {!! Form::label('question1', '1') !!}
        {!! Form::radio('question1', '2') !!}
        {!! Form::label('question1', '2') !!}
        {!! Form::radio('question1', '3') !!}
        {!! Form::label('question1', '3') !!}
        {!! Form::radio('question1', '4') !!}
        {!! Form::label('question1', '4') !!}
        {!! Form::radio('question1', '5') !!}
        {!! Form::label('question1', '5') !!}
    </div>

    <div class="row large-12">
        {!! Form::label('question2', $questionnaire->question2) !!}
        {!! Form::radio('question2', '1') !!}
        {!! Form::label('question2', '1') !!}
        {!! Form::radio('question2', '2') !!}
        {!! Form::label('question2', '2') !!}
        {!! Form::radio('question2', '3') !!}
        {!! Form::label('question2', '3') !!}
        {!! Form::radio('question2', '4') !!}
        {!! Form::label('question2', '4') !!}
        {!! Form::radio('question2', '5') !!}
        {!! Form::label('question2', '5') !!}
    </div>

    <div class="row large-12">
        {!! Form::label('question3', $questionnaire->question3) !!}
        {!! Form::radio('question3', '1') !!}
        {!! Form::label('question3', '1') !!}
        {!! Form::radio('question3', '2') !!}
        {!! Form::label('question3', '2') !!}
        {!! Form::radio('question3', '3') !!}
        {!! Form::label('question3', '3') !!}
        {!! Form::radio('question3', '4') !!}
        {!! Form::label('question3', '4') !!}
        {!! Form::radio('question3', '5') !!}
        {!! Form::label('question3', '5') !!}
    </div>

    <div class="row large-12">
        {!! Form::label('question4', $questionnaire->question4) !!}
        {!! Form::radio('question4', '1') !!}
        {!! Form::label('question4', '1') !!}
        {!! Form::radio('question4', '2') !!}
        {!! Form::label('question4', '2') !!}
        {!! Form::radio('question4', '3') !!}
        {!! Form::label('question4', '3') !!}
        {!! Form::radio('question4', '4') !!}
        {!! Form::label('question4', '4') !!}
        {!! Form::radio('question4', '5') !!}
        {!! Form::label('question4', '5') !!}
    </div>

    <div class="row large-12">
        {!! Form::label('question5', $questionnaire->question5) !!}
        {!! Form::radio('question5', '1') !!}
        {!! Form::label('question5', '1') !!}
        {!! Form::radio('question5', '2') !!}
        {!! Form::label('question5', '2') !!}
        {!! Form::radio('question5', '3') !!}
        {!! Form::label('question5', '3') !!}
        {!! Form::radio('question5', '4') !!}
        {!! Form::label('question5', '4') !!}
        {!! Form::radio('question5', '5') !!}
        {!! Form::label('question5', '5') !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::submit('Submit Response', ['class' => 'button round']) !!}
    </div>
    {!! Form::close() !!}

    </div>

    <!--If condition to check if there is a logged in user
        (This has to be done otherwise the next if condition
        presented an error when a use wasn't logged in)-->
    @if(Auth::check())
    <!--Checks if the current auth id matches the questionnaire creator id-->
    @if(Auth::user()->id == $questionnaire->creator_id)
    <div class="row">
    <a href="{{ $questionnaire->id }}/edit" class="linkbtn button round">Edit this Questionnaire</a>
        <h2 class="responses">Any Responses to your Questionnaire will appear here</h2>
        <table class="responsesTable centered column">
            <tr>
                <th>{{ $questionnaire->question1 }}</th>
                <th>{{ $questionnaire->question2 }}</th>
                <th>{{ $questionnaire->question3 }}</th>
                <th>{{ $questionnaire->question4 }}</th>
                <th>{{ $questionnaire->question5 }}</th>
            </tr>
            @foreach ($response as $responses)
                @if($responses->questionnaire_id == $questionnaire->id)
                    <tr>
                        <td>{{ $responses->question1 }}</td>
                        <td>{{ $responses->question2 }}</td>
                        <td>{{ $responses->question3 }}</td>
                        <td>{{ $responses->question4 }}</td>
                        <td>{{ $responses->question5 }}</td>
                    </tr>
                @endif
            @endforeach
        </table>
    @endif
    @endif
    </div>   
</div>  
<!--Includes the footer in the page-->
<div class="footer">
    @include('includes.footer') 
</div> 
</body>
</html>