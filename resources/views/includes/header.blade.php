<nav class="siteNav">
    <div>
         <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/questionnaires">Questionnaires</a></li>
            @can('see_adminnav')
            <li><a href="/admin/users">Users list</a></li>    
            @endcan
            <li>
            @if (Route::has('login'))
                <div>
                    @auth
                        <a class="navMarginRight" href="{{ url('/home') }}">Your Dashboard | </a> 
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                        </a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                        </form>
                </div>
                    @else
                        <a href="{{ route('login') }}">Login | </a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            </li>
        </ul>
    </div>
</nav>