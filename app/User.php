<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Creates the belongs to many relationship between a questionnaire and a user
     */
    public function questionnaire() 
    {
        return $this->belongsToMany('App\questionnaire');
    }

    /**
     * Creates the belongs to many relationship between a role and a user
     */
    public function roles() 
    {
        return $this->belongsToMany(role::class);
    }

    /**
     * hasRole method to check if a role is attached to a user
     */
    public function hasRole($role)
    {
        if(is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !! $role->intersect($this->roles)->count();
    }

    /**
     * assignRole method to assign a role to a user
     */
    public function assignROle($role) 
    {
        return $this->roles()->sync(
            Role::whereName($role)->firstOrFail()
        );
    }
}
