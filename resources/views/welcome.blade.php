<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the CSS file to the page-->
    <title>Laravel</title>
</head>
<!--Includes the header in the view-->
@include('includes.header')
<body>
<div class="contentContainer">
    <h3>Welcome to this questionnaire site, this applicatiom has been built as part of CW2 for CIS2167 - Server & Client Side Scriptings</h3>
    <br>
    <p>The questionnaires that you are able to produce using the system is limited to 5 questions all of which are answered by a 1-5 scale so bear that in mind when writing questions</p> 
    </div>
<!--Includes the footer in the view-->
<div class="footer">
    @include('includes.footer') 
</div>
</body>
</html>
