<?php 
$I = new FunctionalTester($scenario);

$I->am('an admin');
$I->wantTo('remove a user account from the system');

//Let the test use the login id 1
Auth::loginUsingId(1);

//Add a test user to the system for the test to work with
$I->haveRecord('users', [
    'id' => '999',
    'name' => 'Codecept User',
    'email' => 'codecept@example.com',
    'password' => 'testuser',
]);


//When
$I->amOnPage('/');
$I->see('Users list');

//And
$I->click('Users list', 'a');

//Then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Codecept User');

//And
$I->see('Delete User');
$I->click('Delete User');

//Then
$I->dontSee('Codecept User');
