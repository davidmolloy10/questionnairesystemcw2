<?php 
$I = new FunctionalTester($scenario);

$I->am('a user');
$I->wantTo('test codepcept installed correctly');

//When
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel', 'title');