<?php 
$I = new FunctionalTester($scenario);

$I->am('a registered user');
$I->wantTo('Login to the system');

//When
$I->amOnPage('/');
$I->see('Laravel', 'title');
$I->see('Login', 'a');

//And 
$I->click('Login');

//Then
$I->submitForm('.login',[
    'email' => 'david@example.com',
    'password' => 'password123'
]);

//And
$I->amOnPage('/home');
$I->see('Welcome David Molloy You are logged in!');


