<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{   
    /**
     * Create the belongs to many relationship between a permsision and a role
     */
    public function roles() 
    {
        return $this->belongsToMany(role::class);
    }
}
