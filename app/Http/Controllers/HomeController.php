<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\questionnaire;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Ensures that authentication is required to access this content in this controller
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /**
         * Creates a variable called questionnaires which will get all the questionnaires where the creator_id 
         * field and the current auth::user->id match
         * 
         * This is then returned with the home view
         */
        $questionnaires = questionnaire::where('creator_id', Auth::user()->id)->get();
        return view('home', ['questionnaires' => $questionnaires]);
    }
}
