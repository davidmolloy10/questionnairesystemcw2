<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the CSS file to the page-->
    <title>Questionnaires</title>
</head>
<body>
<!--Includes the header into the page-->
@include('includes.header')
<div class="row contentContainer">
    <h2>Available Questionnaires</h2>
    <!--Checks to see if the page has been loaded with the success attribute from the controller,
        if it has been then the success message is displayed-->
    @if(session('success'))
    <h2>{{session('success')}}</h2>
    @endif
    <!--If the questionnaire data is set, each questionnaire will be displayed in its own card style box to show its title-->
    @if (isset ($questionnaires))
        <div class="questionnaireContainer">
            @foreach ($questionnaires as $questionnaire)
            <!--Checks the questionnaire status is set to live before loading it into this view-->
            @if ($questionnaire->status == 'live')
                <div class="questionnaire">
                    <a href="/admin/questionnaires/{{$questionnaire->id}}">
                        <p>{{ $questionnaire->title }}</p>
                    </a>
                </div> 
            @endif
            @endforeach
    @else
        <!--If no questionnaires belong to the user a simple piece of text is loading saying they haven't made any questionnaires yet-->
        <p>You have no questionnaires yet, try making one</p>
    @endif
        </div>
</div>
<!--Includes the footer in the view--> 
<div class="footer">
    @include('includes.footer') 
</div> 
</body>
</html>