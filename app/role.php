<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{   
    /**
     * Creates the belongs to many relationship between a role and a permission
     */
    public function permissions()
    {
        return $this->belongsToMany(permission::class);
    }

    /**
     * Allows for a permission to be given to a role
     */
    public function givePermissionTo(Permission $permission)
    {
        return $this->permission()->sync($permission);
    }
}
