<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('register to the system');

//When
$I->amOnPage('/');
$I->see('Laravel', 'title');
$I->see('Register', 'a');

//And 
$I->click('Register');

//Then
$I->seeCurrentUrlEquals('/register');
//And
$I->see('Register', 'title');
$I->submitForm('.register',[
    'name' => 'test user',
    'email' => 'testuser@example.com',
    'password' => 'test1234',
    'password-confirm' => 'test1234',
]);

//Then
$I->amOnPage('/home');
$I->see('Welcome test user You are logged in!', 'p');