<?php 
$I = new FunctionalTester($scenario);

$I->am('a researcher');
$I->wantTo('create a questionnaire');

//Let the test use the login id 1
Auth::loginUsingId(1);

//When
$I->amOnPage('/home');
$I->see('Your Dashboard');

//And
$I->click('Create a Questionnaire');

//Then
$I->seeCurrentUrlEquals('/admin/questionnaires/create');
$I->see('Create a New Questionnaire', 'h1');

//And
$I->submitForm('.questionnaireForm', [
    'title' => 'test questionnaire 3',
    'description' => 'this is a test questionnaire for codecept',
    'status' => 'live',
    'question1' => 'Test Question 1',
    'question2' => 'Test Question 2',
    'question3' => 'Test Question 2',
    'question4' => 'Test Question 2',
    'question5' => 'Test Question 2'
]);

//Then
$I->seeCurrentUrlEquals('/home');

//And
$I->see('test questionnaire 3');
$I->see('this is a test questionnaire for codecept');
