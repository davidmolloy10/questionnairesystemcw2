<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the CSS file to the page-->
    <title>Users List</title>
</head>
<body>
<!--Includes the header in the page-->
@include('includes.header')
<div class="row contentContainer">
    <h1>List of all Registered Users</h1>
    <section>
        <!--Checks the users data is loaded before displaying a table-->
        @if (isset ($users))
            <table class="centered column">
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Permissions</th>
                    <th>Delete User</th>
                </tr>
                @foreach ($users as $user)
                    <tr>
                        <td><a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <ul>
                                @foreach($user->roles as $role)
                                    <li>{{ $role->label }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <!--Opens a form with a submit button to call the destroy function in the user controller-->                            
                            {{ Form::open(array('action' => ['UserController@destroy', $user->id], 'method' => 'DELETE')) }}
                                <div class='row'>
                                    {!! Form::submit('Delete User Account', ['class' => 'button alert round']) !!}
                                </div>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>There is currently no users on the system</p>
        @endif
    </section>
</div>
<!--Loads the footer into the page-->
<div class="footer">
    @include('includes.footer')
</div>
</body>
</html>