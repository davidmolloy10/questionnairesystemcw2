<?php 
$I = new FunctionalTester($scenario);

$I->am('a user');
$I->wantTo('submit a response to a questionnaire');

//Let the test use the login id 1
Auth::loginUsingId(1);

//Add a test questionnaire for the test to use
$I->haveRecord('questionnaires', [
    'id' => '999',
    'title' => 'test questionnaire',
    'description' => 'this is a test questionnaire for codecept',
    'status' => 'live',
    'question1' => 'Test Question 1',
    'question2' => 'Test Question 2',
    'question3' => 'Test Question 2',
    'question4' => 'Test Question 2',
    'question5' => 'Test Question 2',
    'creator_id' => '1'
]);

//When
$I->amOnPage('/questionnaires');
$I->see('test questionnaire');

//And
$I->click('test questionnaire', '.questionnaire');

//Then
$I->seeCurrentUrlEquals('/admin/questionnaires/999');
$I->see('test questionnaire');

//And
$I->submitForm('#submitResponse', [
    'question1' => '3',
    'question2' => '1',
    'question3' => '5',
    'question4' => '5',
    'question5' => '2'
]);

//Then
$I->seeCurrentUrlEquals('/questionnaires');

//And
$I->see('Thank you for responding to the survey');
