<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the stylesheet to the webpage-->
    <title>Create a Questionnaire</title>
</head>
<body>
<!--Adds the header to the page-->
@include('includes.header')
<div class="contentContainer">
    <h1>Create a new Questionnaire</h1>

<!--Using the validation from the questionnaire controller, if any point 
    of the validation fails the error will be shown here-->
    @if ($errors->any())
      <div class="row errorBox">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif

    <!--Form for creating a questionnaire-->
    {!! Form::open(array('action' => 'QuestionnaireController@store', 'class' => 'questionnaireForm')) !!}
    @csrf

    <div class="row">
        {!! Form::label('title', 'Questionnaire Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('description', 'Questionnaire Description:') !!}
        {!! Form::textarea('description', null, ['class' => 'large-12 columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('status', 'Questionnaire Status:') !!}
        {!! Form::select('status', array('offline' => 'Offline', 'live' => 'Live'), 'offline'); !!}
    </div>
    
    <div class="row">
        {!! Form::label('question1', 'Question 1:') !!}
        {!! Form::text('question1', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question2', 'Question 2:') !!}
        {!! Form::text('question2', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question3', 'Question 3:') !!}
        {!! Form::text('question3', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question4', 'Question 4:') !!}
        {!! Form::text('question4', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question5', 'Question 5:') !!}
        {!! Form::text('question5', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::submit('Add Questionnaire', ['class' => 'button submitBtn']) !!}
    </div>
    {!! Form::close() !!}
</div>
<!--Adds the foooter the page-->
<div class="footer">
    @include('includes.footer') 
</div> 
</body>
</html>