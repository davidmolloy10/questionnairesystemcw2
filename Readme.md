Contributor = David Molloy. This is a laravel 6 project for CIS2167 CW2 based on making a questionnaire styled system

Should the video demonstration of the application fail to work or show the detail required, the application be be cloned from my repository however, getting it to work on a new system can prove difficult and thus should only be a last resort, the following steps have been tried and do appear to provide a working method, however this could be subject to change on different systems.

1 | Open MySQL workbench and create a new schema called "questionnairesystem", you should give it the Default Collation of "utf-8 default collation", click apply and      close mysql workbench for now

2| Clone the application from the following repository into a suitable location of your choice (https://bitbucket.org/davidmolloy10/questionnairesystemcw2/)

3| Open the application folder in your editor (e.g phpstorm or vscode) and open the file "AuthServiceProvider.php", lines 25-37 (the boot method) should be cut out temporarily 

4| Now you should run the command "composer update" !-Ensure you are in the root folder of the application first-! (This will run for a few minutes)

5| After the composer command has ran, you should check the .env file and ensure the password is correct for your system close the file after this

6| Now run the command php artisan migrate to migrate the database

7| The boot method removed in step 3 should now be pasted back into its same location 

8| Run "php artisan migrate:refresh"

9| Run the command "php artisan db:seed" 

10| Run the command "php artisan serve" and load the site in your browser (http://localhost:8000)

11| Register an account on the system, note the email doesn't matter if it is real or fake

12| Reopen MySQL workbench and select the top 1000 rows from the "role_user" table, here we are going to add 2 records, the first one under role_id enter 1, and under user_id enter 1. The second record should have 2 under role_id and under user_id is 1 again. Then click apply to save it into the database

13| Still in MySQL workbench and select the top 1000 rows from the "permission_role" table, here we are going to add 2 records, the first one under permission_id enter 1, and under role_id enter 1. The second record should have 2 under permission_id and under role_id is 1 again. Then click apply to save it into the database. You can now close MySQL workbench.

14| Refresh the web browswer page and use the system hopefully!