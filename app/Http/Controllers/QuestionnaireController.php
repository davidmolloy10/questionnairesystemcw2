<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\questionnaire;
use App\response;
use Auth;


class QuestionnaireController extends Controller
{   
    /**
     * Sets up the auth for the controller but only on the create, store
     * edit, update and destroy methods, this is because we went the index and show method
     * to be available to unauthenticated users
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Creates a vairable called questionnaires which will retrive all the information from the questionnaires 
         * and passes the data into the questionnaires view.
         */
        $questionnaires = questionnaire::all();
        return view('questionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //The create method returns the admin/questionnaires/create view when called
        return view('admin/questionnaires/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * This validates the input from the user when creating a questionnaire
         * bail simply means that it will stop validating at the first stoppiing point,
         * required meaning the input is required and min/max being the minimum amount and max amount of
         * characters
         */
        $this->validate($request, [
            'title' => 'bail|required|min:3|max:255',
            'description' => 'bail|required|min:3|max:255',
            'status' => 'required',
            'question1' => 'bail|required|min:3|max:255',
            'question2' => 'bail|required|min:3|max:255',
            'question3' => 'bail|required|min:3|max:255',
            'question4' => 'bail|required|min:3|max:255',
            'question5' => 'bail|required|min:3|max:255',
        ]);

        /**
         * Sets the questionnaire variable to the questonnaire model::create function which takes all of the user input
         * The creator_id field is then populated using the auth model by accessing the users id.
         */
        $questionnaire = questionnaire::create($request->all());
        $questionnaire->creator_id = Auth::user()->id;
        
        //Saves the questionnaire variable into the database.
        $questionnaire->save();

        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Retrieves the questionnaire finding the id
        $questionnaire = questionnaire::where('id', $id)->first();

        //Retrieves all of the responses from the response table
        $response = response::all();

        //if questionnaire doesnt exist go back to the list view if it does load the questionnaire with the response data
        if(!$questionnaire) {
            return redirect('questionnaires');
        }
        return view('/admin/questionnaires/show', ['response' => $response])->withquestionnaire($questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * Loads the questionnaire with the matching id that is passed in
         */
        $questionnaire = questionnaire::findOrFail($id);

        //Loads the edit view with the questionnaire data obtained from the previous statement
        return view('admin/questionnaires/edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Create a variable called questionnaire to store the questionnaire data of the passed in ID
         */
        $questionnaire = questionnaire::findOrFail($id);

        /**
         * Uses the questionnaire variable to update the database with all of the data from the the form
         */
        $questionnaire->update($request->all());
        
        //Redirects the user to the questionnaires view
        return redirect('questionnaires');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
