<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['id' => '1', 'name' => 'see_adminnav', 'label' => 'Can see the admin navbar elements'],
            ['id' => '2', 'name' => 'see_all_users', 'label' => 'Can view all the users'],
        ]);
    }
}
