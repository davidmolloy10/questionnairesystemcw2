<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the stylesheet to the webpage-->
    <title>Edit | {{ $questionnaire->title }}  </title> <!--Uses the questionnaire variable passed in to access the title and set that as the page title-->
</head>
<body>
<!--Adds the header to the page-->
@include('includes.header')
<div class="contentContainer">
    <h1>Edit | {{ $questionnaire->title }}</h1>

    <!--Creates a model binded form to load the content into the form for editing-->
    {!! Form::model($questionnaire, ['action' => ['QuestionnaireController@update', $questionnaire->id], 'method' => 'PUT', 'class' => 'questionnaireForm']) !!}
    
    @csrf
    
    <div class="row">
        {!! Form::label('title', 'Questionnaire Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('description', 'Questionnaire Description:') !!}
        {!! Form::textarea('description', null, ['class' => 'large-12 columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('status', 'Questionnaire Status:') !!}
        {!! Form::select('status', array('offline' => 'Offline', 'live' => 'Live')); !!}
    </div>
    
    <div class="row">
        {!! Form::label('question1', 'Question 1:') !!}
        {!! Form::text('question1', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question2', 'Question 2:') !!}
        {!! Form::text('question2', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question3', 'Question 3:') !!}
        {!! Form::text('question3', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question4', 'Question 4:') !!}
        {!! Form::text('question4', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::label('question5', 'Question 5:') !!}
        {!! Form::text('question5', null, ['class' => 'large-8-columns']) !!}
    </div>

    <div class="row">
        {!! Form::submit('Save Edits', ['class' => 'button submitBtn']) !!}
    </div>
    {!! Form::close() !!}
</div>
<!--Adds the footer to the page-->
<div class="footer">
    @include('includes.footer') 
</div> 
</body>
</html>