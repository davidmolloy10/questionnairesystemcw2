<?php

use Illuminate\Database\Seeder;

class QuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionnaires')->insert([
            ['id' => 5, 'title' => "Developing a Useable Survey System", 'description' => "This is a questionnaire all about the creation of systems that are used to aid user research", 'status' => 'live', 'question1' => 'How easy do you feel it would be to develop a survey system?', 'question2' => 'How well do you feel surveys work to aid primary research', 'question3' => 'On a scale of 1-5 how likely are you to respond to a survey', 'question4' => 'How hard do you feel it is to avoid bias in questions? With 1 being very easy to avoid and 5 being very hard to avoid', 'question5' => 'This is just a filler question',   'creator_id' => '1'],
            ['id' => 6, 'title' => "Questionnaire about food choices", 'description' => "This is a questionnaire all about the food choices that people make", 'status' => 'live', 'question1' => 'How many of your 5 a day do you typically eat?', 'question2' => 'How often do you order takeout food instead of cooking?', 'question3' => 'How often do you go out to a resturant for a meal?', 'question4' => 'How likely would you be to use a fresh food delivery service such as "Hello Fresh"?', 'question5' => 'This is just a filler question',   'creator_id' => '1'],
            ['id' => 7, 'title' => "Questionnaire about Technology", 'description' => "This is a questionnaire all about the your opinion on technology companies", 'status' => 'offline', 'question1' => 'What is your opion on the compnay Apple?', 'question2' => 'What is your opion on the compnay Google?', 'question3' => 'What is your opion on the compnay Microsoft?', 'question4' => 'How likely would you be to buy a device from a company you have not used before/in a long time?', 'question5' => 'Would you say you do not like to change from your trusted brands?',   'creator_id' => '1'],
          ]);
    }
}
