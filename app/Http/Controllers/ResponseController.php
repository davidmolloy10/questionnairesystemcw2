<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\questionnaire;
use App\response;
use Auth;

class ResponseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        /**
         * This validates the input from the user when responding to a survey
         * required meaning the input is required
         */
        $this->validate($request, [
            'question1' => 'required',
            'question2' => 'required',
            'question3' => 'required',
            'question4' => 'required',
            'question5' => 'required'
        ]);
        
        /**
         * Creates and sets the response variable to the response models create function which takes the
         * request variable (the input from the user)
         */
        $response = response::create($request->all());

        /**
         * The response variable is then saved
         */
        $response->save();

        /**
         * Finally the user is redirected to the questionnaires page
         * with the additional withSuccess() function, this allows a string to passed into the view
         * that if the view is loaded corretly will then be displayed when being routed to the view through this method
         */
        return redirect('/questionnaires')->withSuccess('Thank you for responding to the survey');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
