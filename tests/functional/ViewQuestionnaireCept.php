<?php 
$I = new FunctionalTester($scenario);

$I->am('a user');
$I->wantTo('view a questionnaire');

//Let the test use the login id 1
Auth::loginUsingId(1);

//Add a test user for the test to use
$I->haveRecord('questionnaires', [
    'id' => '999',
    'title' => 'test questionnaire',
    'description' => 'this is a test questionnaire for codecept',
    'status' => 'live',
    'question1' => 'Test Question 1',
    'question2' => 'Test Question 2',
    'question3' => 'Test Question 2',
    'question4' => 'Test Question 2',
    'question5' => 'Test Question 2',
    'creator_id' => '1'
]);

//When
$I->amOnPage('/questionnaires');
$I->see('test questionnaire');

//And
$I->click('test questionnaire', '.questionnaire');

//Then
$I->seeCurrentUrlEquals('/admin/questionnaires/999');
$I->see('test questionnaire');
