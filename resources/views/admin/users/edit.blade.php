<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--Links the CSS file to the web page-->
    <title>{{ $user->name }}</title> <!--Uses the user variable passed in to use the users name as the page title-->
</head>
<body>
<!--Includes the header in the page-->
@include('includes.header')
<div class="row contentContainer">
    <h1>Edit User | {{ $user->name }}</h1>

    <div class="row userEdit">
    <!--Opens a model binded form to the user model, passing in the content of the required ID-->
    {!! Form::model($user, ['action' => ['UserController@update', $user->id], 'method' => 'PATCH']) !!}

    <div>
        {!! Form::label('name', 'Username:') !!}
        {!! Form::text('name', null) !!}
    </div>

    <div>
        {!! Form::label('email', 'Email Address:') !!}
        {!! Form::text('email', null) !!}
    </div>

    <div>
        {!! Form::label('roles', 'Roles:') !!}
        <!--Runs a foreach to load each role the user has-->
        @foreach($roles as $role)
            {{ Form::label($role->name) }}
            {{ Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id]) }}
        @endforeach
    </div>

    <div>
        {!! Form::submit('Update User') !!}
    </div>

    {!! Form::close()!!}
    </div>
</div>
<!--Include the footer in the page-->
<div class="footer">
    @include('includes.footer')
</div>
</body>
</html>