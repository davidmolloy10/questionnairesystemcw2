<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> <!--LInks the CSS file to the page-->
    <title>Home</title>
</head>
<body>
<!--Includes the header in the view-->
@include('includes.header')
<div class="row contentContainer">
    <h1>Your Dashboard</h1>
    <p>Welcome {{ Auth::user()->name }} You are logged in!</p>
    <h2>Your Questionnaires</h2>
    @if (isset ($questionnaires))
        <div class="questionnaireContainer">
            @foreach ($questionnaires as $questionnaire)
                <div class="questionnaire">
                    <a href="admin/questionnaires/{{$questionnaire->id}}">
                        <p>{{ $questionnaire->title }} | {{ $questionnaire->status }} </p>
                    </a>
                </div> 
            @endforeach
    @else
        <p>You have no questionnaires yet, try making one</p>
    @endif
        </div>
    <!--Opens a form with a single submit button to call the create method in the questionnaire controller-->
    {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'GET')) }}
        <div class='row'>
            {!! Form::submit('Create a Questionnaire', ['class' => 'button round']) !!}
        </div>
    {{ Form::close() }}
</div>
<!--Includes the footer in the view-->
<div class="footer">
    @include('includes.footer') 
</div>
</body>
</html>
