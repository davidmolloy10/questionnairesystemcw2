<?php 
$I = new FunctionalTester($scenario);

$I->am('a researcher');
$I->wantTo('view the responses to one of my questionnaires');

//Let the test use the login id 1
Auth::loginUsingId(1);

//Add a test questionnaire for the test to use
$I->haveRecord('questionnaires', [
    'id' => '999',
    'title' => 'test questionnaire',
    'description' => 'this is a test questionnaire for codecept',
    'status' => 'live',
    'question1' => 'Test Question 1',
    'question2' => 'Test Question 2',
    'question3' => 'Test Question 3',
    'question4' => 'Test Question 4',
    'question5' => 'Test Question 5',
    'creator_id' => '1'
]);

//Add two test responses to see
$I->haveRecord('responses', [
    'id' => '998',
    'questionnaire_id' => '999',
    'question1' => '1',
    'question2' => '4',
    'question3' => '4',
    'question4' => '2',
    'question5' => '5',
]);

$I->haveRecord('responses', [
    'id' => '999',
    'questionnaire_id' => '999',
    'question1' => '2',
    'question2' => '5',
    'question3' => '1',
    'question4' => '1',
    'question5' => '3',
]);

//When
$I->amOnPage('/questionnaires');
$I->see('test questionnaire');

//And
$I->click('test questionnaire', '.questionnaire');

//Then
$I->seeCurrentUrlEquals('/admin/questionnaires/999');
$I->see('test questionnaire');

//and
$I->see('Any Responses to your Questionnaire will appear here');
$I->see('Test Question 1', 'th');
$I->see('Test Question 2', 'th');
$I->see('Test Question 3', 'th');
$I->see('Test Question 4', 'th');
$I->see('Test Question 5', 'th');

//and 
$I->see('1', 'td');
$I->see('4', 'td');
$I->see('4', 'td');
$I->see('2', 'td');
$I->see('5', 'td');