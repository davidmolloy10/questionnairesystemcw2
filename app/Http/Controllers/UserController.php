<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Protects the controller with authentication being required
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    public function index()
    {
        //Pull all the user data from the user table and store it in the users variable
        $users = User::all();

        /**
         * Returns the users index view with the $users variable containing all user data
         */
        return view('admin/users/index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Retrieve the user data
        $user = User::where('id', $id)->first();

        //Retrive the role data
        $roles = role::all();

        //If the user doesn't exist return to the list view if they do then redirect to the edit page witht the user info and roles
        if(!$user)
        {
            return redirect('/admin/users');
        }
        return view('admin/users/edit')->with('user', $user)->with('roles', $roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        /**
         * Finds the user record for the id passed in and 
         * create a roles variable to get the roles that belong to the user
         * this is then synced with the user and roles tables.
         */
        $user = User::findOrFail($id);
        $roles = $request->get('role');

        $user->roles()->sync($roles);

        //return the user to the users list page
        return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**
         * Creates and sets the user variable to the user model finding the user information
         * of the ID passed in
         */
        $user = User::find($id);

        //Deletes the information containted in the user variable from the database
        $user->delete();

        //Redirects the user back to the users list page
        return redirect('/admin/users');
    }
}
